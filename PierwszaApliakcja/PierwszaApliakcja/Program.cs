﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PierwszaApliakcja
{
    class Program
    {
                
        static void Main(string[] args)
        {
            int liczbaPotrzebnaNamDoWyswietlenia;
            float liczbaNiepotrzebna = 1.15f;
            double liczbaDouble = 3.20;
            decimal liczbaDziesietna = 10;
            int wynik, wyniczek = 0;
            bool czyPrawda = true;

            liczbaPotrzebnaNamDoWyswietlenia = 300;

            //wynik = 2;
            wynik = (int)liczbaDziesietna + (int)liczbaDouble;

            wyniczek += (int)liczbaNiepotrzebna;

            Console.WriteLine(liczbaPotrzebnaNamDoWyswietlenia);
            Console.WriteLine(liczbaNiepotrzebna);
            Console.WriteLine(liczbaDouble);
            Console.WriteLine(liczbaDziesietna);

            //instrukcja warunkowa            
            if(wynik > 10)
            {
                Console.WriteLine("Wynik: " + wynik);
                Console.WriteLine("Wyniczek: " + wyniczek);
            }
            else
            {
                Console.WriteLine("No nic z tego");
            }

            if(wynik > 10 && czyPrawda)
            {
                Console.WriteLine("No tak, Prawda");
            }

            Console.ForegroundColor = ConsoleColor.Red;
            string nazwa = "Ma to sEnS";
            if ((liczbaDouble > 100 || czyPrawda) && true) Console.WriteLine(nazwa.ToLower());
            Console.ForegroundColor = ConsoleColor.White;

            //pętle jelitkowskie
            int iterator = 0;
            while(iterator < 10)
            {
                if (iterator == 9)
                    Console.Write(iterator + "?");
                else
                    Console.Write(iterator + ", ");
                iterator++;
                //++iterator;
                //iterator = iterator +1;
                //iterator += 1;
            }

            Console.WriteLine();
            do
            {
                Console.Write(iterator + "-");
                --iterator;
            } while (iterator >= 0);

            Console.WriteLine();
            for (int i = 0; i < 10; ++i )
            {
                Console.Write(i + "");
            }
            Console.WriteLine();

            //tablice
            int[] tablicaInt = { 2, 15 };

            for (int i = 0; i < tablicaInt.Length ; i++ )
            {
                Console.Write(tablicaInt[i] + " ");
            }

            string pole = "Pole";

            //Listy
            List<string> pierwszaLista = new List<string>();

            pierwszaLista.Add("Linia numer 1");
            pierwszaLista.Add("Druga Linia");
            pierwszaLista.Add("Linia numer 1");
            pierwszaLista.Add("Druga Linia");
            pierwszaLista.Add("Linia numer 1");
            pierwszaLista.Add("Druga Linia");

            foreach(string sztringi in pierwszaLista)
            {
                Console.Write(sztringi + ": ");
                Console.WriteLine(sztringi.Length);
            }

            //wynik = dodawanie(2, 2);

            Console.WriteLine("Wynik: " + Dzialania.dodawanie(2, 2));
            Console.WriteLine("Wynik: " + Dzialania.dodawanie(2.5f, 2.5f));
            Console.WriteLine("Wynik: " + Dzialania.dodawanie());

            string[] plik = File.ReadAllLines("C:\\Users\\Dev\\Documents\\c-sharp\\PierwszaApliakcja\\PierwszaApliakcja\\Dane\\ignus.txt");
            string[] plikJeszczeRaz = File.ReadAllLines(@"C:\Users\Dev\Documents\c-sharp\PierwszaApliakcja\PierwszaApliakcja\Dane\ignus.txt");

            Console.WriteLine("_________________________Pliki___________________________");
                       
            Console.WriteLine(plik[13]);

            //foreach (string s in plik)
            //{
            //    Console.WriteLine(s); // wypisanie wszystkich linii
            //    Console.Write(s + "\t");
            //}

            DateTime dataPierwsza = Convert.ToDateTime(plik[10]);

            //var dataDoWyswietlenia = dataPierwsza.ToString();

            string tekst = string.Format("{0} Data z pliku to {1}.", "1." , dataPierwsza);

            string data = plik[10];
            string godzina = plik[11];

            godzina = godzina.Replace("-", ":");            

            string pattern = "ddd-MM-yyyy HH:mm";

            DateTime datoCzas = Convert.ToDateTime(data + " " + godzina);
            Console.WriteLine(datoCzas);
            //if (DateTime.TryParseExact(data + " " + godzina, pattern, null, System.Globalization.DateTimeStyles.None, out datoCzas))
            //{
            //    Console.WriteLine("Zmienione:" + datoCzas);
            //}

            Console.WriteLine(@"Ignus\n to taka\t fujara C:\nintendo");

            string[] pliki = Directory.GetFiles(@"C:\Users\Dev\Documents\c-sharp\PierwszaApliakcja\PierwszaApliakcja\Dane");
            string[] foldery = Directory.GetDirectories(@"C:\Users\Dev\Documents\c-sharp\PierwszaApliakcja\PierwszaApliakcja\Dane");

            string gdzieJestem = Directory.GetCurrentDirectory();
            var coWyzej = Directory.GetParent(gdzieJestem).Exists;

            List<string> pliczek = new List<string>();

            foreach (string s in File.ReadLines(@"C:\Users\Dev\Documents\c-sharp\PierwszaApliakcja\PierwszaApliakcja\Dane\ignus.txt")) 
            {
                pliczek.Add(s);
            }

            File.WriteAllLines(gdzieJestem + "zapisany.txt", pliczek.ToArray());
            
            Console.ReadKey();
        }


    }
}
