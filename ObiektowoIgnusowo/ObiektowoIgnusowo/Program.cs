﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObiektowoIgnusowo
{
    class Program
    {
        static void Main(string[] args)
        {
            Telefon NokiaLumia = new Telefon();

            NokiaLumia.Dziala = true;
            NokiaLumia.Marka = "Nokia";
            NokiaLumia.Model = "Lumia";
            NokiaLumia.RozmiarWCalach = 5.5;

            var SamsungKuki = new Telefon()
            {                
                Dziala = false,
                Marka = "Samsung",
                RozmiarWCalach = 130
            };

            NokiaLumia.Dziala = false;

            var SamsungGalaksy = new Telefon("Samsung", "Galaxy");
            
            Console.WriteLine("Moja sieć to " + Telefon.PodajSieć(NokiaLumia.Marka));

            Console.ReadKey();
        }
    }
}
