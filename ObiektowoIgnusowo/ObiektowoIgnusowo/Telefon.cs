﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObiektowoIgnusowo
{
    class Telefon
    {
        public string Marka { get; set; }
        public string Model { get; set; }
        public double RozmiarWCalach { get; set; }
        public bool Dziala { get; set; }

        public Telefon() { }

        public Telefon (string Marka, string model)
        {
            this.Marka = Marka;
            Model = model;
            Dziala = true;

            PrzedstawSie();
        }

        public void PrzedstawSie()
        {
            Console.WriteLine("Nazywam się " + Marka + " " + Model);
        }

        public static string PodajSieć(string marka)
        {
            if(marka == "Nokia")
            {
                return "Żabianka";
            }
            return "Gdańsk";
        }
    }
}
